def checkio(data):
    data.sort()
    list_len = int(len(data)//2)
    if len(data) % 2 == 0:
        return (data[list_len-1]+data[list_len])/2
    else:
        return data[list_len]